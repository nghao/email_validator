package sheridan;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * 
 * @author hao nguyen - 991521091
 *
 */

public class EmailValidator {
	private EmailValidator() {
		
	}
	
	public static boolean isValidEmail(String email) {
		Pattern pattern = Pattern.compile("^(.+)@(.+)\\.(.+)$");
		Matcher matcher = pattern.matcher(email);

		// Check email format
		if(!matcher.find()) {
			return false;
		}		

		// Check number of @
		if(occurenceNumberOfCharacter('@', email) != 1) {
			return false;
		}

		// Check account
		if(!isValidAccount(matcher.group(1))) {
			return false;
		}

		// Check domain
		if(!isValidDomain(matcher.group(2))) {
			return false;
		}

		// Check extension
		return isValidExtension(matcher.group(3));
	}

	public static int occurenceNumberOfCharacter(char character, String str) {

		return (int) str.chars().filter(ch -> ch == character).count();
	}

	public static boolean isValidAccount(String account) {
		Pattern pattern = Pattern.compile("^[a-z]{3,}.*$");
		Matcher matcher = pattern.matcher(account);

		return matcher.find();
	}

	public static boolean isValidDomain(String domain) {
		Pattern pattern = Pattern.compile("[a-z0-9]{3,}");
		Matcher matcher = pattern.matcher(domain);

		return matcher.find();
	}

	public static boolean isValidExtension(String extension) {
		Pattern pattern = Pattern.compile("^[a-zA-Z]{2,}$");
		Matcher matcher = pattern.matcher(extension);

		return matcher.find();
	}
}
