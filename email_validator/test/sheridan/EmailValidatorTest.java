package sheridan;

/**
 * 
 * @author hao nguyen - 991521091
 *
 */

import static org.junit.Assert.*;

import org.junit.Test;

public class EmailValidatorTest {
	static final String ATSIGNWARNING = "Invalid. Email can only have one @ sign.";
	static final String ACCOUNTWARNING = "Invalid. Account have at least 3 alpha-characters in lowercase (must not start with a number).";
	static final String DOMAINWARNING = "Invalid. Domain have at least 3 alpha-characters in lowercase or numbers.";
	static final String EXTENSIONWARNING = "Invalid. Extension have at least 2 alpha-characters (no numbers).";
	static final String EMAILWARNING = "Invalid email.";

	// Test if email has only one @ sign
	@Test 
	public void emailHasOnlyOneAtSignRegular( ) {
		assertTrue(  ATSIGNWARNING , EmailValidator.isValidEmail("hao@sheridancollege.ca"));
	}

	@Test 
	public void emailHasOnlyOneAtSignBoundIn( ) {
		assertTrue(  ATSIGNWARNING , EmailValidator.isValidEmail("hao@gmail.com"));
	}

	@Test 
	public void emailHasOnlyOneAtSignException( ) {
		assertFalse(  ATSIGNWARNING , EmailValidator.isValidEmail(" "));
	}

	@Test 
	public void emailHasOnlyOneAtSignBoundaryOut( ) {
		assertFalse(  ATSIGNWARNING , EmailValidator.isValidEmail("hao@@sheridancollege.ca"));
	}



	// Test if account is valid
	@Test 
	public void accountIsValidRegular( ) {
		assertTrue(  ACCOUNTWARNING , EmailValidator.isValidAccount("hao123"));
	}

	@Test 
	public void accountIsValidBoundIn( ) {
		assertTrue(  ACCOUNTWARNING , EmailValidator.isValidAccount("hao"));
	}

	@Test 
	public void accountIsValidException( ) {
		assertFalse(  ACCOUNTWARNING , EmailValidator.isValidAccount("!#$%^&*"));
	}

	@Test 
	public void accountIsValidBoundOut( ) {
		assertFalse(  ACCOUNTWARNING , EmailValidator.isValidAccount("1ha"));
	}



	// Test if domain is valid
	@Test 
	public void domainIsValidRegular( ) {
		assertTrue(  DOMAINWARNING , EmailValidator.isValidDomain("sheridancollege"));
	}

	@Test 
	public void domainIsValidBoundIn( ) {
		assertTrue(  DOMAINWARNING , EmailValidator.isValidDomain("4ch"));
	}

	@Test 
	public void domainIsValidException( ) {
		assertFalse(  DOMAINWARNING , EmailValidator.isValidDomain("!#$%^&*"));
	}

	@Test 
	public void domainIsValidBoundaryOut( ) {
		assertFalse(  DOMAINWARNING , EmailValidator.isValidDomain("ha"));
	}



	// Test if extension is valid
	@Test 
	public void extensionIsValidRegular( ) {
		assertTrue(  EXTENSIONWARNING , EmailValidator.isValidExtension("com"));
	}

	@Test 
	public void extensionIsValidBoundIn( ) {
		assertTrue(  EXTENSIONWARNING , EmailValidator.isValidExtension("ca"));
	}

	@Test 
	public void extensionIsValidException( ) {
		assertFalse(  EXTENSIONWARNING , EmailValidator.isValidExtension(""));
	}

	@Test 
	public void extensionIsValidBoundaryOut( ) {
		assertFalse(  EXTENSIONWARNING , EmailValidator.isValidExtension("a"));
	}



	// Test if email is valid
	@Test 
	public void emailIsValidRegular( ) {
		assertTrue(  EMAILWARNING , EmailValidator.isValidEmail("hao89@sheridancollege.ca"));
	}

	@Test 
	public void emailIsValidBoundIn( ) {
		assertTrue(  EMAILWARNING , EmailValidator.isValidEmail("hao@sheridan.college.ca"));
	}

	@Test 
	public void emailIsValidException( ) {
		assertFalse(  EMAILWARNING , EmailValidator.isValidEmail("123-456-7890"));
	}

	@Test 
	public void emailIsValidBoundaryOut( ) {
		assertFalse(  EMAILWARNING , EmailValidator.isValidEmail("89hao@@sheridancollege.ca"));
	}

}
